from django.urls import path, include
from .views import BlogHomePageView, BlogDetailView, BlogCreateView, BlogEditView, BlogDeleteView

urlpatterns = [
    path('', BlogHomePageView.as_view(), name='homeblog'),
    path('post/<int:pk>/', BlogDetailView.as_view(), name='detail_blog'),
    path('delete/<int:pk>/', BlogDeleteView.as_view(), name='post_delete'),
    path('post/new/', BlogCreateView.as_view(), name='post_new'),
    path('post/edit/<int:pk>', BlogEditView.as_view(), name='post_edit'),
]