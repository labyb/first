from django.views.generic import ListView
from .models import Post

# Create your views here.


class HomePostView(ListView):
    model = Post
    template_name = 'homepost.html'
    context_object_name = 'all_posts_list'
